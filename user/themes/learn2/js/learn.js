// Scrollbar Width function
function getScrollBarWidth() {
    var inner = document.createElement('p');
    inner.style.width = "100%";
    inner.style.height = "200px";

    var outer = document.createElement('div');
    outer.style.position = "absolute";
    outer.style.top = "0px";
    outer.style.left = "0px";
    outer.style.visibility = "hidden";
    outer.style.width = "200px";
    outer.style.height = "150px";
    outer.style.overflow = "hidden";
    outer.appendChild(inner);

    document.body.appendChild(outer);
    var w1 = inner.offsetWidth;
    outer.style.overflow = 'scroll';
    var w2 = inner.offsetWidth;
    if (w1 == w2) w2 = outer.clientWidth;

    document.body.removeChild(outer);

    return (w1 - w2);
};

function setMenuHeight() {
    $('#sidebar .highlightable').height($('#sidebar').innerHeight() - $('#header-wrapper').height() - 40);
}

function fallbackMessage(action) {
    var actionMsg = '';
    var actionKey = (action === 'cut' ? 'X' : 'C');

    if (/iPhone|iPad/i.test(navigator.userAgent)) {
        actionMsg = 'No support :(';
    }
    else if (/Mac/i.test(navigator.userAgent)) {
        actionMsg = 'Press ⌘-' + actionKey + ' to ' + action;
    }
    else {
        actionMsg = 'Press Ctrl-' + actionKey + ' to ' + action;
    }

    return actionMsg;
}

function convertTextareaToMarkdownEditor(textarea){
    return new SimpleMDE({ 
                element: textarea,
                forceSync: true,
                toolbar: ["heading", "|", "bold", "italic", "|", "unordered-list", "|", "preview"]
            });
}

// for the window resize
$(window).resize(function() {
    setMenuHeight();
});

// debouncing function from John Hann
// http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
(function($, sr) {

    var debounce = function(func, threshold, execAsap) {
        var timeout;

        return function debounced() {
            var obj = this, args = arguments;

            function delayed() {
                if (!execAsap)
                    func.apply(obj, args);
                timeout = null;
            };

            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);

            timeout = setTimeout(delayed, threshold || 100);
        };
    }
    // smartresize
    jQuery.fn[sr] = function(fn) { return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery, 'smartresize');


jQuery(document).ready(function() {
    var sidebarStatus = searchStatus = 'open';

    // set the menu height
    setMenuHeight();

    jQuery('#overlay').on('click', function() {
        jQuery(document.body).toggleClass('sidebar-hidden');
        sidebarStatus = (jQuery(document.body).hasClass('sidebar-hidden') ? 'closed' : 'open');

        return false;
    });

    jQuery('.scrollbar-inner').scrollbar();

    jQuery('[data-sidebar-toggle]').on('click', function() {
        jQuery(document.body).toggleClass('sidebar-hidden');
        sidebarStatus = (jQuery(document.body).hasClass('sidebar-hidden') ? 'closed' : 'open');

        return false;
    });
    jQuery('[data-clear-history-toggle]').on('click', function() {
        sessionStorage.clear();
        location.reload();
        return false;
    });
    jQuery('[data-search-toggle]').on('click', function() {
        if (sidebarStatus == 'closed') {
            jQuery('[data-sidebar-toggle]').trigger('click');
            jQuery(document.body).removeClass('searchbox-hidden');
            searchStatus = 'open';

            return false;
        }

        jQuery(document.body).toggleClass('searchbox-hidden');
        searchStatus = (jQuery(document.body).hasClass('searchbox-hidden') ? 'closed' : 'open');

        return false;
    });

    var ajax;
    jQuery('[data-search-input]').on('input', function() {
        var input = jQuery(this),
            value = input.val(),
            items = jQuery('[data-nav-id]');

        items.removeClass('search-match');
        if (!value.length) {
            $('ul.topics').removeClass('searched');
            items.css('display', 'block');
            sessionStorage.removeItem('search-value');
            $(".highlightable").unhighlight({ element: 'mark' })
            return;
        }

        sessionStorage.setItem('search-value', value);
        $(".highlightable").unhighlight({ element: 'mark' }).highlight(value, { element: 'mark' });

        if (ajax && ajax.abort) ajax.abort();
        ajax = jQuery.ajax({
            url: input.data('search-input') + ':' + value
        }).done(function(data) {
            if (data && data.results && data.results.length) {
                items.css('display', 'none');
                $('ul.topics').addClass('searched');
                data.results.forEach(function(navitem) {
                    jQuery('[data-nav-id="' + navitem + '"]').css('display', 'block').addClass('search-match');
                    jQuery('[data-nav-id="' + navitem + '"]').parents('li').css('display', 'block');
                });
            }
            ;

        });
        jQuery('[data-search-clear]').on('click', function() {
            jQuery('[data-search-input]').val('').trigger('input');
            sessionStorage.removeItem('search-input');
            $(".highlightable").unhighlight({ element: 'mark' })
        });
    });

    if (sessionStorage.getItem('search-value')) {
        jQuery(document.body).removeClass('searchbox-hidden');
        jQuery('[data-search-input]').val(sessionStorage.getItem('search-value'));
        jQuery('[data-search-input]').trigger('input');
    }

    // clipboard
    var clipInit = false;
    $('code').each(function() {
        var code = $(this),
            text = code.text();

        if (text.length > 5) {
            if (!clipInit) {
                var text, clip = new Clipboard('.copy-to-clipboard', {
                    text: function(trigger) {
                        text = $(trigger).prev('code').text();
                        return text.replace(/^\$\s/gm, '');
                    }
                });

                var inPre;
                clip.on('success', function(e) {
                    e.clearSelection();
                    inPre = $(e.trigger).parent().prop('tagName') == 'PRE';
                    $(e.trigger).attr('aria-label', 'Copied to clipboard!').addClass('tooltipped tooltipped-' + (inPre ? 'w' : 's'));
                });

                clip.on('error', function(e) {
                    inPre = $(e.trigger).parent().prop('tagName') == 'PRE';
                    $(e.trigger).attr('aria-label', fallbackMessage(e.action)).addClass('tooltipped tooltipped-' + (inPre ? 'w' : 's'));
                    $(document).one('copy', function(){
                        $(e.trigger).attr('aria-label', 'Copied to clipboard!').addClass('tooltipped tooltipped-' + (inPre ? 'w' : 's'));
                    });
                });

                clipInit = true;
            }

            code.after('<span class="copy-to-clipboard" title="Copy to clipboard" />');
            code.next('.copy-to-clipboard').on('mouseleave', function() {
                $(this).attr('aria-label', null).removeClass('tooltipped tooltipped-s tooltipped-w');
            });
        }
    });

    
    // allow keyboard control for prev/next links
    jQuery(function() {
        jQuery('.nav-prev').click(function(){
            location.href = jQuery(this).attr('href');
        });
        jQuery('.nav-next').click(function() {
            location.href = jQuery(this).attr('href');
        });
    });

    jQuery(document).keydown(function(e) {
      // prev links - left arrow key
      if(e.which == '37') {
        jQuery('.nav.nav-prev').click();
      }

      // next links - right arrow key
      if(e.which == '39') {
        jQuery('.nav.nav-next').click();
      }
    });     

});

jQuery(window).on('load', function() {

    function adjustForScrollbar() {
        if ((parseInt(jQuery('#body-inner').height()) + 83) >= jQuery('#body').height()) {
            jQuery('.nav.nav-next').css({ 'margin-right': getScrollBarWidth() });
        } else {
            jQuery('.nav.nav-next').css({ 'margin-right': 0 });
        }
    }

    // adjust sidebar for scrollbar
    adjustForScrollbar();

    jQuery(window).smartresize(function() {
        adjustForScrollbar();
    });

    // store this page in session
    sessionStorage.setItem(jQuery('body').data('url'), 1);

    // loop through the sessionStorage and see if something should be marked as visited
    for (var url in sessionStorage) {
        if (sessionStorage.getItem(url) == 1) jQuery('[data-nav-id="' + url + '"]').addClass('visited');
    }


    $(".highlightable").highlight(sessionStorage.getItem('search-value'), { element: 'mark' });

    
    // === LUDOPOLI
    // Project Plan form
    jQuery('form input, form textarea').on('input', function() {
        var input = jQuery(this),
            value = input.val();

        var sessionVarName = input.attr('name');

        if (!value.length) {

            sessionStorage.removeItem(sessionVarName);
            return;
        }

        sessionStorage.setItem(sessionVarName, value);

    });
    jQuery('form input, form textarea').each(function(index,elem){

            var $input = $(elem),
            sessionVarName = $input.attr('name');

            if (sessionStorage.getItem(sessionVarName)) {
                $input.val(sessionStorage.getItem(sessionVarName));
                $input.trigger('input');
            }

            // Convert our textareas to SimpleMDE editors
            if(elem.tagName.toLowerCase() === 'textarea'){
                if($input.hasClass('ismarkdown')){

                    var mde = new SimpleMDE({ 
                        element: elem,
                        toolbar: false,
                        status: false,
                        placeholder: ' '
                    });

                    //! This is important. Bind the mde instance to a data attribute on our field.
                    // We use this for our inline fields
                    $input.data('MdeInstance', mde);

                    SimpleMDE.togglePreview(mde);

                    // Custom change event to trigger the session store function
                    // This should be happending automatically, but isn't..
                    mde.codemirror.on("change", function(e){

                        var elem = mde.element;
                        var source = $(elem);

                        SimpleMDE.togglePreview(mde);

                        source.val(mde.value());
                        source.trigger('input');
                    });
                }
            }
    });


    $('data').each(function(index,elem){

        $e = $(elem);
        if($e.hasClass('js-inlinefield')){
            createProjectPlanInlineField($e);
        }

        $e.remove();
    });


    // View Switches
    $('button.viewswitch').on('click',function(e){
        $btn = $(e.currentTarget);

        var mode = 
            $btn.data('mode');

        switchViewMode(mode);
    })

    //-- end LUDOPOLI

});

// Project Planner : Inline Fields
function createProjectPlanInlineField(element){

    var $elem = $(element);
    if($elem !== undefined && $elem.hasClass('js-inlinefield')){

        var fieldId = $elem.attr('value');
        if(fieldId && fieldId.length > 0){
            // get the field from the actual form
            var field = document.getElementById(fieldId);
            if(field && field !== undefined){
                // basically just clone it.
                var $clone = $(field).clone().appendTo(($elem).parent());
                if($clone && $clone !== undefined){

                    $clone
                    .attr('id', fieldId + '-inline')
                    .data('sourcefield', fieldId);

                    // create new mde editor
                    var inlineMde = convertTextareaToMarkdownEditor($clone[0]);

                    // get the source doc and link it, 
                    // see: http://codemirror.net/doc/manual.html#api_doc

                    // pull related mde from data attribute on input field
                    var sourceMde = $(field).data('MdeInstance');
                    if(sourceMde !== undefined){
                        var sourceDoc = sourceMde.codemirror.getDoc();
                        var newDoc = sourceDoc.linkedDoc({'sharedHist':true});
                        inlineMde.codemirror.swapDoc(newDoc);
                    }
                    else {
                        console.warn('souceMde instance is undefined');
                    }
                }
                else {
                    console.error('could not clone textarea for js-inlinefield');
                }
                
                
            }
            else {
                console.warn('cannot find related field for js-inlinefield. value = ' + fieldId);
            }
        }

    }

} //== LUDOPOLI function

function switchViewMode(mode){
    //modes=
    //  guide-planner
    //  guideonly
    //  planneronly

    $body = $('body');

    switch(mode){
        case 'guideplanner':
            $body.removeClass('guideonly planneronly');
            $body.addClass('guideplanner');

            $('#plan-form textarea').each(function(e){
                $e = $(e.currentTarget);
                $('a.fa-eye', $e).click();
            });

            break;
        case 'guideonly':
            $body.removeClass('guideplanner planneronly');
            $body.addClass('guideonly')
            break;
        case 'planneronly':
            $body.removeClass('guideplanner guideonly');
            $body.addClass('planneronly')
            break;
        default:
            break;
    }
} //== LUDOPOLI function


$(function() {
    $('a[rel="lightbox"]').featherlight({
        root: 'section#body'
    });
});

jQuery.extend({
    highlight: function(node, re, nodeName, className) {
        if (node.nodeType === 3) {
            var match = node.data.match(re);
            if (match) {
                var highlight = document.createElement(nodeName || 'span');
                highlight.className = className || 'highlight';
                var wordNode = node.splitText(match.index);
                wordNode.splitText(match[0].length);
                var wordClone = wordNode.cloneNode(true);
                highlight.appendChild(wordClone);
                wordNode.parentNode.replaceChild(highlight, wordNode);
                return 1; //skip added node in parent
            }
        } else if ((node.nodeType === 1 && node.childNodes) && // only element nodes that have children
            !/(script|style)/i.test(node.tagName) && // ignore script and style nodes
            !(node.tagName === nodeName.toUpperCase() && node.className === className)) { // skip if already highlighted
            for (var i = 0; i < node.childNodes.length; i++) {
                i += jQuery.highlight(node.childNodes[i], re, nodeName, className);
            }
        }
        return 0;
    }
});

jQuery.fn.unhighlight = function(options) {
    var settings = {
        className: 'highlight',
        element: 'span'
    };
    jQuery.extend(settings, options);

    return this.find(settings.element + "." + settings.className).each(function() {
        var parent = this.parentNode;
        parent.replaceChild(this.firstChild, this);
        parent.normalize();
    }).end();
};

jQuery.fn.highlight = function(words, options) {
    var settings = {
        className: 'highlight',
        element: 'span',
        caseSensitive: false,
        wordsOnly: false
    };
    jQuery.extend(settings, options);

    if (!words) { return; }

    if (words.constructor === String) {
        words = [words];
    }
    words = jQuery.grep(words, function(word, i) {
        return word != '';
    });
    words = jQuery.map(words, function(word, i) {
        return word.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    });
    if (words.length == 0) { return this; }
    ;

    var flag = settings.caseSensitive ? "" : "i";
    var pattern = "(" + words.join("|") + ")";
    if (settings.wordsOnly) {
        pattern = "\\b" + pattern + "\\b";
    }
    var re = new RegExp(pattern, flag);

    return this.each(function() {
        jQuery.highlight(this, re, settings.element, settings.className);
    });
};
