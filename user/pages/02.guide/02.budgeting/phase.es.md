---
title: Presupuestación del proyecto
subtitle: Fase 2 
menu: Presupuestación
cache_enable: true
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
taxonomy:
    category:
        - docs
---
Preguntas sobre el presupuesto de tu proyecto.