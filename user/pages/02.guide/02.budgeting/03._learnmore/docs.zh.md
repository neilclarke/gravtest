---
title: 想了解更多？
displaytitle: 请戳以下连接
taxonomy:
    category: docs
hasinlinefield: 'false'
---

-	国际预算伙伴关系——非政府组织预算工作指南（5种语言）
-	微软Office软件——预算模板
-	项目管理视频——项目成本管理建议（视频）
-	世界银行——世界银行预算编制建议
