---
title: Want to learn more?
menu: Learn more
slug: _learnmore
taxonomy:
    category: docs
hasinlinefield: 'false'
---
-  <a target="_blank" href="www.internationalbudget.org/publications/a-guide-to-budget-work-for-ngos">International Budget Partnership</a>   
	A guide for NGO budget work (5 languages)
-  <a href="https://templates.office.com/en-au/Budgets" target="_blank">Microsoft Office</a>       
	Budget templates
-	<a target="_blank" href="https://www.youtube.com/watch?v=oXhgwn-girI">Project Management videos</a>   
	Project cost management tips (video) 
-	<a target="_blank" href="siteresources.worldbank.org/INTBELARUS/Resources/Budgeting.pdf">World Bank</a>     
	Budgeting tips from World Bank 
