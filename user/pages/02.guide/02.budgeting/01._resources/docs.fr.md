---
title: Quelles sont les ressources nécessaires à la réalisation de votre projet ?
taxonomy:
    category: docs
---

Un bon budget doit être aussi précis que possible et recenser les ressources, les quantités et les coûts. Tout d’abord, il est indispensable de déterminer quelles sont les ressources physiques nécessaires, et dans quelle quantité, afin d’atteindre les objectifs que vous avez fixés. Ces ressources peuvent être classées par catégories, par exemple ressources humaines, équipements techniques, matériels/fournitures, communications/publications. Faites la liste de tout ce dont vous aurez besoin et en quelle quantité, en essayant d’être le plus précis possible