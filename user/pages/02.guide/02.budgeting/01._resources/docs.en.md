---
title: What resources are required to implement your project?
taxonomy:
    category: docs
---

A good budget is as specific as possible and puts together resources, quantities and costs. Firstly, it is important to determine what physical resources are needed and what quantities (the amount) of each are required in order to reach the objectives you established. These resources can be broken down into different categories such as human resources, technical equipment, materials/supplies, communications/publications (mouse over with different types of resources/inputs). Develop a list of what you will need and the amount, and try to be as precise as possible