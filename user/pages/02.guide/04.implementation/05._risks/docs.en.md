---
title: 'What could go wrong in your project and how would you handle it?'
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

Basically, this refers to risk management. It is very likely, that you will face some obstacles and hurdles while implementing your project. Though not everything can be controlled, it is important to identify what risks could affect the project and to have a plan on how you will respond to them (just in case). Risks can range from being political, technical or financial. Therefore, try to list events or aspects that could potentially affect your project, and determine what can be done to mitigate them as well as potential response mechanisms.
 