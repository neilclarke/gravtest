---
title: ¿Qué podría salir mal y cómo lo afrontarías?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Esto se refiere esencialmente a la gestión de riesgos. Es muy probable que cuando lleves a cabo el proyecto, te encuentres con algunos obstáculos. Aunque no se puede controlar todo, es importante conocer qué riesgos podrían afectar al proyecto y tener un plan de respuesta. Los riesgos pueden ser políticos, técnicos o financieros. Trata de hacer una lista de los aspectos que pueden afectar el proyecto y determinar qué se puede hacer para mitigarlos, así como los posibles mecanismos de respuesta.