---
title: ما هو وجه الخلل الذي قد يطرأ على مشروعك وكيف ستعالجه إن حدث؟ 
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
يتعلق هذا السؤال بالأساس بمسالة إدارة المخاطر. فمن المرجح جداً أنك ستواجه بعض العقبات والمعوقات أثناء تنفيذك للمشروع. وعلى الرغم من أنه ليس من السهل التحكم بجميع مفاصل وجوانب وجزئيات المشروع، إلا أن من المهم تحديد المخاطر التي قد تلحق الضرر بالمشروع ووضع خطة لكيفية التصدي لها ومعالجتها (من باب التحوط). وتتنوع هذه المخاطر بين سياسية وتقنية ومالية. لذلك حاول أن تحدد الأحداث أو الجوانب التي قد تنطوي على إمكانية التأثير سلباً على مشروعك، ومعرفة ما ينبغي عمله للتخفيف من وطأة هذه المخاطر وكذلك معرفة آليات الاستجابة المحتملة.