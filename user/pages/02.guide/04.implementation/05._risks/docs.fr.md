---
title: Quelles sont les difficultés susceptibles de surgir au cours du projet, comment y faire face ?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Nous parlerons ici de gestion du risque (mouseover with a risk definition). Au cours de la mise en œuvre de votre projet, il est très probable que vous vous heurtiez à des obstacles et à des difficultés. Même s’il est impossible de tout maîtriser, il est indispensable de déterminer quels sont les risques qui pourraient compromettre le projet et de prévoir des solutions (juste au cas où). Les risques peuvent être d’ordre politique, technique ou financier. Essayez par conséquent de dresser la liste des événements ou des éléments qui pourraient nuire à votre projet et réfléchissez à des solutions qui permettraient d’atténuer ces risques ainsi qu’à des mécanismes de réaction potentiels.