---
title: 'What other stakeholders are involved?'
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

Your project will not be implemented in isolation. There will be external stakeholders participating, whether it be the community where you are working, the donors who are funding your project or the government authorities. Keep in mind that you must coordinate with them in order to ensure adequate project implementation. Therefore, take the time to identify all the stakeholders involved, their relationship with the project and how you will work with them throughout the implementation. 