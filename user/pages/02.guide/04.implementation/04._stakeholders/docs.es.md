---
title: ¿Qué otros interesados participan?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Tu proyecto no se ejecutará aisladamente. Habrán interesados externos que participen, ya sea la comunidad en la que actúas, los donantes que financian tu proyecto o las autoridades. Ten presente que tienes que coordinar con ellos para ejecutar correctamente el proyecto. Toma el tiempo necesario para averiguar quiénes son todos los interesados, sus relaciones con el proyecto y cómo trabajarás con ellos durante la ejecución.