---
title: Ces activités peuvent-elles être réalisées selon les délais fixés et dans les limites du budget ? 
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

Un contrôle financier approprié et une gestion du temps efficace en vue d’atteindre les objectifs du projet sont deux des principales caractéristiques d’une bonne gestion de projet. Vous veillerez à ce que les tâches à effectuer s’inscrivent dans les limites du budget que vous avez établi. Au début de la mise en œuvre du projet, assurez-vous que les dépenses n’entraînent pas de dépassement du budget. Réexaminez sans cesse votre budget. Vous vous souvenez des délais que vous avez fixés pour la réalisation du projet ? Il s’agit à présent d’y intégrer les tâches que vous venez de définir afin d’établir un calendrier de travail, en prévoyant le moment auquel vous effectuerez chacune des tâches ainsi que leur durée. Lorsque la mise en œuvre du projet démarre, servez-vous du calendrier pour vérifier ce qui a été fait ou reste à faire et confirmer que votre projet ne prend pas de retard. Vous serez ainsi en mesure de suivre la progression de votre projet. 