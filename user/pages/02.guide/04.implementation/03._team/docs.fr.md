---
title: Qui seront les membres de votre équipe, que feront-ils ?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Un projet n’est pas toujours une entreprise individuelle. Si vous faites partie d’une organisation, alors d’autres membres de cette organisation participeront sans doute au projet. Vous pouvez également faire en sorte que votre organisation travaille en partenariat avec une autre organisation (ou plusieurs). Au début du projet, il est donc indispensable de définir les rôles (qui fait quoi) et les responsabilités (qui prend les décisions, qui est responsable de quoi) de chacun des membres de l’équipe. Il est également nécessaire de désigner le chef de projet, qui devra veiller à ce que tout se déroule sans heurt. Enfin, si le projet exige des compétences dont votre organisation ne dispose pas, il faudra peut-être envisager de recruter des personnes supplémentaires.