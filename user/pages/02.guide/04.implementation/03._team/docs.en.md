---
title: 'Who will be in your team and what will they be doing?'
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

A project is not always an individual endeavor. If you are part of an organization, then probably other members will participate in the project. Another possibility is that your organization will partner and work together with another organization (or with various organizations). Therefore, before the project begins it is important to define the roles (who does what) and responsibilities (who decides what or is accountable for what) of each team member.  Also, it is necessary to clarify who is the project manager, as this person will ensure that all project processes are working smoothly together. Finally, consider that you might need to hire additional people if the project requires specific skills not found in your organization.