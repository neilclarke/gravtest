---
title: ¿Quién formará parte de tu equipo y qué hará cada quién?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Un proyecto no siempre es una iniciativa individual. Si formas parte de una organización, es probable que otros miembros de ella participen en el proyecto. Otra posibilidad es que tu organización se alíe y coopere con otra (o con varias organizaciones). Por lo tanto, antes que empiece el proyecto es importante definir las funciones (quién hace qué) y las responsabilidades (quién decide qué o es responsable de qué) de cada miembro del equipo. Asimismo, es necesario aclarar quién es el gerente del proyecto, ya que esta persona cuidará que todos los procesos del proyecto se desarrollen en conjunto y sin tropiezos. Por último, considera que más personas podrían ser contratadas si el proyecto necesita competencias que tu organización no posee.