---
title: من الذي سيكون في فريقك وما الذي سيقومون بعمله؟
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
المشروع ليس دائماً مسعى فردياً. إذا كنت جزءاً من منظمة، فربما سيشارك أعضاء آخرون في المشروع. الاحتمال الاخر هو أن منظمتك ستدخل في مشاركة مع منظمة أخرى وتعمل سوية معها (أو مع عدة منظمات). لذلك من المهم أن تُحدد قبل بدء المشروع أدوار (مَن يقوم بماذا) ومسؤوليات (من الذي يقرر وماذا يقرر أو من المسؤول عن ماذا) كل عضو من أعضاء الفريق. ومن الضروري كذلك تحديد من هو مدير المشروع، إذ تقع على عاتق هذا الشخص مسؤولية تنسيق وتنظيم عمل المشروع على نحو يضمن أن تعمل جميع عمليات المشروع معاً بسلاسة واتساق. وأخيراً، ضع في اعتبارك أنك قد تحتاج إلى الاستعانة بأشخاص إضافيين إذا تطلب المشروع مهارات لا تتوفر في فريقك أو مؤسستك.