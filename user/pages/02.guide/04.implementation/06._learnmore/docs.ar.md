---
title: هل تريد معرفة المزيد؟
menu: هل تريد معرفة المزيد؟
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---
وصلات للأدوات والمبادئ التوجيهية

-  <a target="_blank" href="https://nonprofits.fb.com/topic/ask-people-to-speak-out/#define-actions-to-take">Facebook</a>   
	كيفية تنفيذ دعوة إلى العمل في الفيسبوك
-  <a href="https://www.youtube.com/watch?v=ADK58IRPKh8" target="_blank">Gantt Chart</a>       
	ما هو مخطط غانت، وكيف يستخدم لتخطيط وجدولة المشروع
-	<a target="_blank" href="https://templates.office.com/en-us/Timelines">Microsoft Office</a>   
	نماذج التقاويم والجداول الزمنية
-	<a target="_blank" href="https://images.template.net/wp-content/uploads/2016/07/27083817/Team-Assignment-Template.doc">Project Connections</a>     
	نموذج تنظيم الأفرقة وتوزيع المهام
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">UN Women</a>   
	كيف تبدأ تنفيذ مشروعك- الصفحات 117-120
-	<a target="_blank" href="www.wikihow.com/Sample/Gantt-Chart">WikiHow</a>         
	نموذج مخطط غانت لإكسيل
-	<a target="_blank" href="https://www.youtube.com/watch?v=TjxL_hQn5w0">Youtube</a>         
	كيفية وضع مخطط غانت بسيط (فيديو تعليمي)