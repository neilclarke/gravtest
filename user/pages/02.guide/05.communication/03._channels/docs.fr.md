---
title: Quels moyens de communication utiliserez-vous ?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

Une fois définis votre public cible et vos objectifs, vous devrez choisir les moyens (le comment) qui vous permettront de transmettre vos messages clés de la façon la plus efficace possible et de toucher votre public cible. Les possibilités sont variées : médias sociaux, sites Web, presse écrite. Les moyens utilisés dépendront du type de public. Afin de vous démarquer, faites preuve de créativité lorsque vous décrirez l’intérêt que revêt votre projet. De même, vous devrez déterminer à quel moment et avec quelle fréquence envoyer vos messages (le quand). Tenez aussi compte de vos limites, qu’il s’agisse de contraintes budgétaires ou de difficultés d’accès à une technologie donnée. Attention, le moyen le plus efficace de toucher votre public n’est pas nécessairement le plus cher. Enfin, la communication demande que l’on y consacre beaucoup de temps, notez qu’il vous sera peut-être nécessaire de confier cette tâche à plein temps à un membre de l’équipe.