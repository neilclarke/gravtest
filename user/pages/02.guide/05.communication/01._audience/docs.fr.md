---
title: Qui sont les personnes cibles, pourquoi sont-elles importantes ?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

Communiquer, pourquoi ? Il est essentiel de faire savoir ce que vous faites afin que toutes les personnes associées au projet en soient informées mais aussi pour que le travail que vous accomplissez gagne en visibilité aux yeux des différentes parties prenantes. Votre but, c’est que votre projet soit connu! Mais vous voulez aussi que l’on comprenne pourquoi votre projet est important et quels changements il entraîne, vous souhaitez même inciter d’autres personnes à s’y impliquer. Afin de diffuser vos informations, vous devez d’abord définir les différents types de public que vous ciblez : donateurs, pouvoirs publics au niveau local ou national, société civile/communauté, autres organisations de jeunesse. Réfléchissez aussi aux raisons pour lesquelles ils sont importants, autrement dit, pourquoi il est important de leur fournir des informations sur votre projet.