---
title: ما هو جمهورك المستهدف وما أهميته؟
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
لماذا التواصل والاتصال؟ إن من الضروري أن يكون جميع المشاركين في المشروع وجميع الأطراف المعنية به على علم بما تعمل من خلال مشاطرة ونشر المعلومات المتعلقة بسير المشروع وتسليط الضوء عليه وإبراز أنشطته. هذا لأنك تريد أن يكون الناس على علم بوجود مثل هذا المشروع وأن يدركوا أيضاً أهميته، والتغييرات التي يولدها، بل لأنك تريد كذلك أن تشجعهم على المشاركة فيه. ولكي تشاطر المعلومات، يتعين عليك أولاً تحديد جمهورك المستهدف. والجمهور المعني أصناف تشمل: الجهات المانحة، والسلطات المحلية والوطنية، والمجتمع المدني أو المجتمع المحلي، ومنظمات شبابية أخرى. ويتعين عليك أيضاً أن تبين أهمية الجمهور المستهدف، أي لماذا من المهم أن يكون هذا الجمهور على علم بمشروعك ومجرياته.