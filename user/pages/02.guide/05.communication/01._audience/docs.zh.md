---
title: 你的目标受众是谁？为什么是他们？
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

介绍和宣传你的项目，对于参与项目的各方了解情况、推动利益相关方关注你开展的工作，都是很重要的。另一方面，你也希望人们能够理解这个项目为什么重要，将带来哪些改变，希望鼓励人们参与进来。要分享项目信息，你必须先设定目标受众。不同类型的受众包括捐助方、当地或国家主管部门、民间社团、其他青年组织。你还必须明确为什么会选中他们，为什么必须让这些人了解项目情况。