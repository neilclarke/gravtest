---
title: Project Communications and Visibility
subtitle: Phase 5
menu: Communications
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
taxonomy:
    category: docs
---
Questions about generating visibility and communicating about your project.
