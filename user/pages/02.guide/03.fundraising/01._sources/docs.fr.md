---
title: Quelles sont les différentes sources de financements nécessaires à la réalisation de votre projet ? 
taxonomy:
    category: docs
---

Ҫa y est, vous avez un plan et un budget. Étape suivante : comment trouver des financements pour concrétiser votre projet ? Il est essentiel de rechercher plusieurs sources de financement en fonction de l’objectif de votre projet et de sa thématique. Les donateurs peuvent être très divers et comprendre le secteur privé (entreprises), des fondations et des organisations internationales. En outre, votre communauté propose peut-être des possibilités de financement spécifiques à l’initiative des autorités locales ou nationales. Soyez organisé lorsque vous lancez vos recherches, commencez au niveau local (municipalité/province) avant de passer au niveau national puis au niveau international. Mais vous pouvez aussi faire appel à d’autres sources de financement telles que le crowdfunding ou les événements locaux de collecte de fonds. 