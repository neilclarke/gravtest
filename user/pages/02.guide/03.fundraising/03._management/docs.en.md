---
title: How will you manage the funds?
taxonomy:
    category: docs
---

In order to obtain funding from certain institutions, you will usually be required to fill out applications and in many cases compete with other project proposals. Drafting applications for funding takes time and dedication. Before applying, read carefully the requirements. If you fulfill all of them, then draft the application. If you can, have another person look over your application for feedback. You want to have the best application possible! When you draft a fundraising plan, include the different sources of funding, the application requirements, and the deadlines. You should also take into consideration how much time it will take between drafting the applications and actually receiving the funds.