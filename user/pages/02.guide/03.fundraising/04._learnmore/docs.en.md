---
title: Want to learn more?
menu: Learn More
slug: _learnmore
taxonomy:
    category: docs
hasinlinefield: 'false'
---
-  <a target="_blank" href="https://nonprofits.fb.com/raise-funds/">Facebook</a>   
	How to raise funds on Facebook
-  <a href="https://www.firstinspires.org/resource-library/fundraising-toolkit" target="_blank">FIRST Lego League</a>       
	Fundraising in six steps (videos)
-	<a target="_blank" href="www.youthassembly.nyc/fundraising-guide/">The Youth Assembly at the United Nations</a>   
	Grassroot fundraising strategy 
-	<a target="_blank" href="www.unicef.ca/sites/default/files/legacy/imce_uploads/images/ce/events/fundraising_ideas_8_dec_2014.pdf">UNICEF</a>     
	Fundraising activity ideas 

