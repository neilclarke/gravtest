---
title: Collecte de fonds
subtitle: Étape 3
menu: Collecte de fonds
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
process:
    markdown: false
taxonomy:
    category: docs
---
Questions concernant la recherche des financements nécessaires à la réalisation de votre projet.
