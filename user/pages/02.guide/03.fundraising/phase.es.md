---
title: Recaudación de fondos para el proyecto
subtitle: Fase 3
menu: Recaudación de fondos
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
process:
    markdown: false
taxonomy:
    category: docs
---
Preguntas sobre cómo obtener los fondos necesarios para ejecutar tu proyecto.