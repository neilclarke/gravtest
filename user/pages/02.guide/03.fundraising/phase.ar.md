---
title: المرحلة الثالثة: جمع التبرعات للمشروع
menu: المرحلة الثالثة: جمع التبرعات للمشروع
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
process:
    markdown: false
taxonomy:
    category: docs
---
أسئلة بشأن الحصول على الأموال اللازمة لتنفيذ مشروعك.
