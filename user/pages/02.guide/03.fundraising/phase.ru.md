---
title: Привлечение средств на осуществление проекта
subtitle: Этап 3
menu: Привлечение средств на осуществление проекта
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
process:
    markdown: false
taxonomy:
    category: docs
---
Вопросы, касающиеся изыскания средств, необходимых для осуществления вашего проекта: