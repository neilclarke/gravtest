---
title: Quel est le problème auquel vous voulez remédier ?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Pour réaliser un bon projet, il faut d’abord bien poser le problème. Il est essentiel en premier lieu de décrire le problème auquel vous voulez remédier, dans votre pays, dans votre communauté ou dans votre école. De façon générale, décrivez ce que vous voulez changer et pourquoi. Pour vérifier si votre description est suffisamment claire, essayez de résumer le problème en une seule phrase.