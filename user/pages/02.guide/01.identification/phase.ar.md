---
title: المرحلة الأولى: تحديد المشروع وتخطيطه
menu: المرحلة الأولى: تحديد المشروع وتخطيطه
image: phase-1-image.png
jumbo: phase-1-jumbo.png
cache_enable: true
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
process:
    markdown: false
taxonomy:
    category:
        - docs
---
أسئلة بشأن المشروع في مرحلة تحديده وتخطيطه