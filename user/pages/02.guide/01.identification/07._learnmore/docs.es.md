---
title: ¿Quieres saber más?
menu: ¿Quieres saber más?
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---
Enlaces a instrumentos y guías

-  <a target="_blank" href="https://project-proposal.casual.pm">Casual</a>   
	Manual para elaborar propuestas de proyectos (con plantillas y muestras)
-  <a href="https://www.youtube.com/watch?v=jsGBuu88WE0" target="_blank">Project Management videos</a>       
	Cómo redactar una propuesta de proyecto (vídeo)
-	<a target="_blank" href="https://strategyzer.com/books/business-model-generation#order">Strategyzer</a>   
	Manual para la generación de un modelo de negocio
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">ONU Mujeres</a>     
	Consejos de ONU Mujeres para elaborar un plan de proyecto - págs. 68-90 
-	<a target="_blank" href="http://www.wikihow.com/Start-a-Nonprofit-Organization">WikiHow</a>   
	Cómo poner en marcha una ONG
-	<a target="_blank" href="http://www.wikihow.com/Write-a-Business-Proposal">WikiHow</a>         
	Cómo redactar una propuesta de negocio