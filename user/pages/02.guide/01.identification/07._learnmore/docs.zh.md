---
title:  想了解更多？
taxonomy:
    category: docs
process:
	twig: true
hasinlinefield: 'false'
---
- 	请戳以下连接
-	Casual——项目提案工具包（配有模板和样本）
-	项目管理视频——如何撰写项目提案（视频）
-	战略决策者——商业模式启动手册
-	联合国妇女署——《妇女署教你如何制定明晰的项目计划》，第68-90页
-	WikiHow——如何创建一家非政府组织
-	WikiHow——如何撰写商业提案