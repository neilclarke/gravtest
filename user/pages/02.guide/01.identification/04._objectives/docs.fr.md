---
title: Quels sont les objectifs et/ou les résultats attendus de votre projet ?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Sur quoi portera votre projet ? Si vous avez déjà défini le problème, c’est le moment de réfléchir aux objectifs que vous voulez atteindre, et à la façon dont vous vous proposez d’y parvenir. Notez bien qu’il n’est pas nécessaire de trouver des solutions adaptées à tous les aspects du problème. Soyez réaliste. Mieux vaut se limiter à quelques objectifs (2 à 4) que vous aurez réellement des chances d’atteindre 