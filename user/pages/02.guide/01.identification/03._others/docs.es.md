---
title: ¿Qué otras iniciativas se han llevado a cabo que apunten al mismo problema?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Es probable que otras organizaciones de jóvenes ya hayan hecho proyectos similares para abordar el problema que has identificado. Antes de plantear tu proyecto, investiga qué otras iniciativas se han llevado a cabo sobre el mismo tema. Esto también te permitirá aprender sobre resultados anteriores, así como los aprendizajes y los retos de otros proyectos. Puedes también obtener nuevas ideas que mejoren el diseño de tu proyecto.