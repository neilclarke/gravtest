---
title: ¿Qué duración tendrá tu proyecto?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Es importante determinar cuánto tiempo llevará alcanzar los objetivos establecidos. Un proyecto tiene una duración claramente definida y se tiene que hacer todo lo posible por no sobrepasarla. Para establecerla (puede variar entre tres meses a un par de años), intenta pensar cuántas personas habrá en tu equipo, cuánto tardarás en obtener la financiación necesaria para poner en marcha el proyecto y el tiempo que te llevará coordinar con los diferentes interesados que participan en el proyecto.