---
title: من الذين سيساعدهم مشروعك؟
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
من المهم معرفة من الذين سيدعمهم مشروعك وما نوع وحجم الفائدة التي ستعود عليهم بفضل هذا المشروع. وهل هذه الفائدة تعود على مجموعة من الناس أو على مجتمع محلي أو على رقعة جغرافية محددة؟ والأهم من ذلك، معرفة كيف يلبي مشروعك احتياجاتهم حقاً؟ لا تفترض أنك تعرف كل شيء عن المستفيدين من المشروع. ينبغي أن تتحدث إليهم وتطرح اسئلة، إذ سيساعدك هذا الأمر على تحديد أهداف مشروعك بصورة أفضل وتحسين تصميمه. وهي أيضاً طريقة لضمان دعم المجتمع المحلي لمشروعك والاهتمام به والمشاركة فيه.