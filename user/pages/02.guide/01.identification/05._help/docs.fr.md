---
title: Qui seront les bénéficiaires de votre projet ?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Il est essentiel d’identifier les bénéficiaires de votre projet et de déterminer quel intérêt ce projet présente pour eux. Votre projet aura-t-il des effets bénéfiques pour un groupe de personnes en particulier, une communauté, une région géographique ? Et surtout, en quoi votre projet répond-il vraiment aux besoins des bénéficiaires ? Ne croyez pas tout savoir des bénéficiaires, allez leur parler, posez-leur des questions, cela vous aidera à mieux définir vos objectifs ainsi qu'à améliorer l’élaboration de votre projet. Cette démarche est également un moyen de garantir l’implication, l’intérêt et la participation de la communauté.