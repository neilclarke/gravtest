---
title: 'Want to learn more?'
menu: 'Learn More'
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---
-  <a target="_blank" href="evaluationtoolbox.net.au/index.php?option=com_content&view=article&id=20&Itemid=159">Evaluation Toolbox</a>   
	Develop a monitoring plan in 7 easy steps
-  <a href="https://www.google.ca/intl/en/nonprofits/learning/getting-started.html#tab5" target="_blank">Google</a>       
	How to use Google Analytics for monitoring
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">UN Women</a>   
	Project monitoring according to UN Women - pp. 126-146
-	<a target="_blank" href="https://en.wikipedia.org/wiki/Monitoring_and_evaluation">Wikipedia</a>     
	What is Monitoring and Evaluation (M&E)? 
-	<a target="_blank" href="www.who.int/tdr/publications/year/2014/9789241506960_workbook_eng.pdf">World Health Organization</a>   
	How to build a monitoring and evaluation plan -  p. 100-104