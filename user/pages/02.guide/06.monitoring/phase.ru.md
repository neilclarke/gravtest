---
title: Мониторинг, отчетность и оценка проекта
subtitle: Этап 6
menu: Мониторинг, отчетность и оценка проекта
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
taxonomy:
    category: docs
---
Вопросы, касающиеся мониторинга и оценки вашего проекта: