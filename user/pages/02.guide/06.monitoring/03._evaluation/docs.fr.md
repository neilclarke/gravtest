---
title: Comment consigner et évaluer les résultats du projet ?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

Une fois que le projet touche à sa fin, il vous incombe (ainsi qu’à votre équipe) de noter par écrit les résultats du projet. Essayez de répondre de la manière la plus détaillée possible à la question : quels sont les résultats du projet et comment ont-ils été obtenus ? Comme nous l’avons indiqué plus haut, les donateurs exigent parfois qu’on leur remette des rapports de projet mais, même si ce n’est pas le cas, vous avez tout intérêt à en rédiger un. Les rapports constituent des outils de communication qu’il est utile de diffuser auprès de la communauté et des pouvoirs publics au niveau national et local. Ce sont également de précieuses contributions aux projets futurs.

C’est habituellement à la fin du projet que l’on procède aux évaluations. Les évaluations ont pour objet de déterminer si le projet a produit des effets ou des changements significatifs, ce qui n’est généralement mesurable que sur la durée. C’est la raison pour laquelle les évaluations sont recommandées dans le cas des projets d’une durée supérieure à deux ans au moins. Si le projet dure longtemps, envisagez de mener une évaluation. Cette opération a une incidence sur le plan financier car il est nécessaire d’engager un expert externe pour diriger l’évaluation, ce qui se répercutera sur votre budget.