---
title: Suivi et évaluation
subtitle: Étape 6
menu: Suivi et évaluation
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
taxonomy:
    category: docs
---
Questions concernant le suivi et l’évaluation de votre projet.
