---
title: Project Monitoring, Reporting and Evaluation
subtitle: Phase 6
menu: Monitoring
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
taxonomy:
    category: docs
---
Questions about monitoring and evaluating your project.
