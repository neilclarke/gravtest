---
title: المرحلة السادسة: رصد المشروع وإعداد تقارير بشأنه وتقييمه
menu: المرحلة السادسة: رصد المشروع وإعداد تقارير بشأنه وتقييمه
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
taxonomy:
    category: docs
---
أسئلة بشأن رصد مشروعك وتقييمه
