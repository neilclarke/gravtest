---
title: Conseils pratiques pour l’action des jeunes
menu: Accueil
slug: home
template: home
headerimage: home-header.svg
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
---
# Ces Conseils pratiques pour l’action des jeunes ont pour but de vous aider à passer d’une idée à un plan de projet réalisable. 