---
title: Homepage Features
menu: Getting started
features:
    - title: ¿Por qué Claves para la Acción Juvenil?
      text: Cualquiera puede diseñar e implementar un proyecto. Si has detectado un problema en tu comunidad y tienes una idea sobre cómo resolverlo o abordarlo, puedes llevar a cabo un proyecto. Hemos elaborado una serie de consejos para la Acción Juvenil que pueden ayudarte a transformar tu idea en un plan de proyecto que pueda hacerse realidad. ¿Cómo? Hemos reunido en un solo lugar instrumentos, información y recursos adicionales para que te resulte más fácil diseñar y ejecutar proyectos centrados en generar un impacto social. Al final, habrás redactado tu plan de proyecto. Esencialmente, tú aportas las ideas, y nosotros te ayudamos a estructurarlas. ¿Comenzamos?
      image: home-feature-one.png
---