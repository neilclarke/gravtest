---
title: Homepage Features
menu: Getting started
features:
    - title: Des conseils pratiques pour l’action des jeunes, pourquoi ? 
      text: Concevoir un projet et le réaliser, c’est à la portée de tout le monde. Si vous constatez un problème dans votre communauté et que vous avez une idée sur la façon de le résoudre ou d’y faire face, vous pouvez vous aussi réaliser un projet. Ces Conseils pratiques pour l’action des jeunes ont pour but de vous aider à passer d’une idée à un plan de projet réalisable. Comment ? Nous avons réuni ici des outils, des informations et d’autres ressources pour vous aider à concevoir et à mettre en œuvre des projets visant à produire un impact social. À la fin du processus, vous aurez rédigé votre plan de projet. Pour faire simple, vous venez avec vos idées, et nous vous aidons à tout mettre en place. Vous êtes prêts ?
      image: home-feature-one.png
---