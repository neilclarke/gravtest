---
title: Claves para la Acción Juvenil
menu: Home
slug: home
template: home
headerimage: home-header.svg
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
---
# Hemos elaborado una serie de consejos para la Acción Juvenil que pueden ayudarte a transformar tu idea en un plan de proyecto que pueda hacerse realidad.