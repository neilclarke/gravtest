---
title: ¿Cómo funciona Claves para la Acción Juvenil? 
chartimage: phase-chart-sample.png
---
Claves para la Acción Juvenil se divide en seis secciones que abarcan las distintas fases del ciclo de un proyecto. En cada sección, encontrarás información básica y aspectos a tomar en cuenta por medio de una serie de orientaciones y preguntas para hacerte reflexionar. Por último, al final de cada etapa, hay enlaces a diferentes recursos, instrumentos y guías donde podrás encontrar más detalles. 