---
title: How does Top Tips for Youth Action work?
chartimage: phase-chart-sample.png
---
Top Tips for Youth Action is divided into six sections that encompass the project cycle, which refers to the different phases of a project. 

In each section, you will find basic information and aspects to keep in mind through a series of guidance and self-reflection questions. 

Finally, at the end of each stage, there are links to different resources, tools and guidelines that can provide you with further details.