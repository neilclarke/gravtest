---
title: What is a project?
menu: What is a project?
---
### A project can be defined as a temporary endeavor, or series of activities, undertaken to achieve a specific result.

Projects are constrained by limited resources and also have a specific duration, and therefore have a clear beginning and end.