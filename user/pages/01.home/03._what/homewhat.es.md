---
title: ¿Qué es un proyecto?
---
### Un proyecto se puede definir como un esfuerzo temporal o una serie de actividades, que se realizan para obtener un resultado específico.

Los proyectos son temporales, por lo que, tienen un comienzo y un fin establecido, y también recursos definidos.