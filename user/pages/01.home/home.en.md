---
title: Top Tips for Youth Action
menu: Home
slug: home
template: home
headerimage: home-header.svg
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
---
# We’ve elaborated Top Tips for Youth Action to help you move from an idea to an actionable project plan.