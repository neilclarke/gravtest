---
title: Cómo innovar en la gestión de un proyecto
menu: La gestión de un proyecto
---
La gestión de un proyecto también puede recurrir a distintos enfoques, metodologías e instrumentos alternativos (“no tradicionales”) para abordar más adecuadamente el problema identificado y alcanzar mejor los objetivos del proyecto. Por medio de diversos mecanismos de innovación podemos tratar de hallar soluciones más eficaces a diferentes problemas sociales. A continuación, figuran tres ideas que puedes tomar en cuenta cuando reflexiones sobre tu proyecto.

## Diseño centrado en las personas

Cuando diseñamos un proyecto, inicialmente solemos plantearnos un problema desde nuestra perspectiva; en cambio, el diseño centrado en los seres humanos sitúa al usuario final o al beneficiario en el centro del proyecto. Este mecanismo se basa en realmente entender cuáles son sus necesidades, suscitar empatía y concebir soluciones que atiendan esas necesidades. Este proceso iterativo suele llevar a soluciones más fundamentadas y ayuda a crear confianza. Puede ser una manera de mejorar el diseño de tu proyecto y de establecer vínculos con la comunidad en la que deseas trabajar.

## Ensayos y prototipos

En los proyectos, deberíamos dejar un espacio para hacer ensayos y aprender, antes de comprometer todo nuestro tiempo y recursos financieros a una actividad. Porque, ¿y si no funciona? Podría ser conveniente probar una determinada actividad a pequeña escala para constatar los resultados, obtener enseñanzas del proceso y luego implementarla a mayor escala. También se podrían probar diferentes alternativas, para ahorrar tiempo y recursos

## Micro-mecenazgo (“crowdfunding”)

Un mecanismo de captación de fondos en línea que recurre a la “multitud” para recaudar fondos para diversas iniciativas. Llevar a cabo una campaña de micro-mecenazgo con buenos resultados conlleva tiempo y requiere esfuerzos, pero podría ser una opción interesante para conseguir fondos para tu proyecto.

¿Quieres saber más?  Estos son dos instrumentos de gestión integral de proyectos que podrían ayudarte a organizar y a gestionar tu proyecto:

<section id="section_learnmore" class="question">
	<h2 id="_learnmore">¿Quieres saber más?</h2>
		<div class="section-body">
		<ul>
			<li><a target="_blank" href="https://asana.com/try?noredirect">Asana</a><br>
			Organizador de proyectos en colaboración en línea (gratuito)</li>
			<li><a href="https://products.office.com/fr-ca/project/project-and-portfolio-management-software?tab=tabs-1" target="_blank">Microsoft Project</a><br>
			Instrumento de gestión de proyectos</li>
		</ul>
		</div>
</section>